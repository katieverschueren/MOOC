const notes = [{
    title: 'My next trip',
    body: 'I would like to go to New York.'
}, {
    title: 'Happits to work on',
    body: 'Exercise. Eating a bit better.'
}, {
    title: 'Office modifications',
    body: 'Get a new seat.'
}];

const sortNotes = function(notes){
    notes.sort(function(a, b){
        if(a.title.toLowerCase() < b.title.toLowerCase()){
            return -1;
        } else if (b.title.toLowerCase() < a.title.toLowerCase()) {
            return 1;
        } else {
            return 0;
        }
    });
};

const findNote = function(notes, noteTitle){
    return notes.find(function(note, index) {
       return note.title.toLowerCase() === noteTitle.toLowerCase();
    });
};

const findNotes = function(notes, query){
    return notes.filter(function (note, index) {
        const isTitleMatch = note.title.toLowerCase().includes(query.toLowerCase())
        const isBodyMatch = note.body.toLowerCase().includes(query.toLowerCase())
        return isTitleMatch || isBodyMatch
    });
}

sortNotes(notes);
console.log(notes);

// console.log(findNotes(notes, 'work'));

// const findNote = function (notes, noteTitle) {
//     const index = notes.findIndex(function (note) {
//         return note.title.toLowerCase() === noteTitle.toLowerCase()
//     });
//     return notes[index];
// };

// const note = findNote(notes, 'Office modifications');
// console.log(note);


// for (let count = 2; count >= 0; count--) { // Count ++ is hetzelfde als Count = Count + 1 // Terugtellen is -- ipv ++
//     console.log(count);
// };

// for (let count = notes.length - 1; count >= 0; count--){
//     console.log(notes[count]);
// };


// const index = notes.findIndex(function(note, index){ // findIndex zoekt naar de eerste match
//     console.log(note);
//     return note.title === 'Happits to work on';
// });

// console.log(index);