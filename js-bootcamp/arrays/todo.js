const toDo = [{
    text: 'Onderzoek',
    completed: true
}, {
    text: 'MOOC',
    completed: false
}, {
    text: 'Boodschappen',
    completed: true
}, {
    text: 'Werken',
    completed: false
}, {
    text: 'Slapen',
    completed: true
},
];

const deleteToDo = function (toDo, toDoText) {
    const index = toDo.findIndex(function(todo){
        return todo.text.toLowerCase() === toDoText.toLowerCase()
    });
    if (index > -1){
        toDo.splice(index, 1);
    }
};

const getThingsToDo = function(toDo) {
    return toDo.filter(function(todo) {
        return todo.completed === false;
    })
}

const sortToDos = function(toDo) {
    toDo.sort(function(a, b){
        if (a.completed === false && b.completed === true){
            return -1;
        } else if (b.completed === false && a.completed === true){
            return 1;
        } else {
            return 0;
        }
    });
}

sortToDos(toDo);
console.log(toDo);

// console.log(getThingsToDo(toDo));

// deleteToDo(toDo, 'Onderzoek');
// console.log(toDo);