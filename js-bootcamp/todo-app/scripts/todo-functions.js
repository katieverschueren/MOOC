'use strict' 

// Fetch existing todos from localStorage
const getSavedTodos = () => {
    const toDoJSON = localStorage.getItem('toDo');

    try {
        return toDoJSON ? JSON.parse(toDoJSON) : []; 
    } catch (e) {
       return []; 
    };
};

// Save todos to localStorage
const saveTodos = (toDo) => {
    localStorage.setItem('toDo', JSON.stringify(toDo));
};

// Remove todo by id
const removeTodo = (id) => {
    const todoIndex = toDo.findIndex((todo) => todo.id === id);

    if (todoIndex > -1){
        toDo.splice(todoIndex, 1);
    }
};

// Toggle the completed value for a given todo
const toggleTodo = (id) => {
    const todo = toDo.find((todo) => todo.id === id);

    if (todo){
        todo.completed = !todo.completed;
    }
};

// Render applications todos based on filters
const renderToDos = (todos, filters) =>  {
    const todoEl = document.querySelector('#todos');
    let filteredTodos = toDo.filter((todo) => {
        return todo.text.toLowerCase().includes(filters.searchText.toLowerCase())
    });

    filteredTodos = filteredTodos.filter(function (todo) {
        if (filters.hideCompleted) {
            return !todo.completed;
        } else {
            return true; // When it is not checked. 
        }
    });

    const incompletedTodos = filteredTodos.filter((todo) => !todo.completed);

    todoEl.innerHTML = '';
    todoEl.appendChild(generateSummaryDOM(incompletedTodos));

    if(filteredTodos.length > 0){
        filteredTodos.forEach((todo) => {
            todoEl.appendChild(generateTodoDOM(todo));
        });
    } else {
        const messageEl = document.createElement('p');
        messageEl.classList.add('empty-message');
        messageEl.textContent = 'No to-dos to show';
        todoEl.appendChild(messageEl);
    };
};

// Get the DOM elements for an individual note
const generateTodoDOM = (todo) => {
    const todoEl = document.createElement('label');
    const containerEl = document.createElement('div');
    const checkbox = document.createElement('input');
    const todoText = document.createElement('span');
    const removeButton = document.createElement('button');

    // Setup todo checkbox
    checkbox.setAttribute('type', 'checkbox');
    checkbox.checked = todo.completed;
    containerEl.appendChild(checkbox);
    checkbox.addEventListener('change', () => {
        toggleTodo(todo.id);
        saveTodos(toDo);
        renderToDos(toDo, filters);
    });

    // Setup the todo text
    todoText.textContent = todo.text;
    containerEl.appendChild(todoText);

    // Setup container
    todoEl.classList.add('list-item');
    containerEl.classList.add('list-item__container');
    todoEl.appendChild(containerEl);

    // Setup the remove button
    removeButton.textContent = 'remove';
    removeButton.classList.add('button', 'button--text');
    todoEl.appendChild(removeButton);
    removeButton.addEventListener('click', () => {
        removeTodo(todo.id);
        saveTodos(toDo);
        renderToDos(toDo, filters);
    });
    
    return todoEl;
};

// Get the DOM elements for list summary
const generateSummaryDOM = (incompletedTodos) => {
    const summary = document.createElement('h2');
    const plural = incompletedTodos.length === 1 ? '' : 's';
    summary.classList.add('list-title');
    summary.textContent = `You have ${incompletedTodos.length} todo${plural} left.`;
    return summary;
};