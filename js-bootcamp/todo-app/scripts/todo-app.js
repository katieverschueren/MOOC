'use strict' 

let toDo = getSavedTodos();

const filters = {
    searchText: '',
    hideCompleted: false
};

renderToDos(toDo, filters);

document.querySelector('#search-text').addEventListener('input', (e) =>{
    filters.searchText = e.target.value;
    renderToDos(toDo, filters);
});

document.querySelector('#new-todo').addEventListener('submit', (e) => {
    const text = e.target.elements.text.value.trim();
    e.preventDefault();

    if(text.length > 0){
        toDo.push({
            id: uuidv4(),
            text,
            completed: false
        });
        saveTodos(toDo);
        renderToDos(todos, filters);
        e.target.elements.text.value = '';
    };
});

document.querySelector('#hide-completed').addEventListener('change', (e) =>{
    filters.hideCompleted = e.target.checked; // Set hideCompleted to true when checkboxes checked and to false when its not.
    renderToDos(todos, filters);
});


