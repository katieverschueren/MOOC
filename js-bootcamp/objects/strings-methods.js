let name = ' Katie Verschueren ';

// Length Property
console.log(name.length);

// Convert To Upper Case 
console.log(name.toUpperCase());

// Convert To Lower Case
console.log(name.toLowerCase());

// Includes Method
let password = 'abc123hardstyle098';
console.log(password.includes('password'));

// Trim Method
console.log(name.trim());


// Challenge Area
let isValidPassword = function (password){
    if (password.length > 8 && password.includes('password')) {
        return true; 
    } else {
        return false; 
    }
}

console.log(isValidPassword('thisismypassword123456'));
console.log(isValidPassword('hoihey'));
console.log(isValidPassword('abcd1234$%^$#@'));