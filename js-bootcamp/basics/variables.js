// 1. You cant define a variable more than once. 
let petName = 'Mitcha';
petName = 'Lellie';

console.log(petName);

// 2. There are rules related to variable names.
let test_ = 2;
let result = 3 + 4;

// 3. Variable names cannot be reserved keywords.
// let let = 12;
