// === - equality operator
// !== - no equal operator
// < - less than operator
// > - greater than operator
// <= - less than or equal to operator
// >= - greater than or equal to operator

let temp = 120;

if (temp <= 32) {
    console.log('It is freezing outside!')
}

if(temp >= 110){
    console.log('It is way to hot outside!')
}

//Challenge
let age = 7;

if(age <= 7){
    console.log('Super Sale! Only for children!')
}

if(age >= 65){
    console.log('Super Senior Discount!')
}