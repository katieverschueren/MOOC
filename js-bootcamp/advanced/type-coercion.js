// Type Coercion - a string, a number, a boolean
const value = false + 12; // False = -1, True = +1
const type = typeof value;
console.log(type);
console.log(value);