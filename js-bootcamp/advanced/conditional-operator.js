/*
const myAge = 20;
const message = myAge >= 18 ? 'You can vote!' : 'You can not vote!';
console.log(message);
*/

const myAge = 20;
const showPage = () => {
    return 'Showing the page';
};
const showErrowPage = () => {
    return 'Showing the errow page';
};

const message = myAge >= 20 ? showPage() : showErrowPage();
console.log(message);

const team = ['Katie', 'Cezan', 'Cas', 'Jos', 'Timon'];
console.log(team.length <= 4 ? `Team size: ${team.length}` : 'Too many people on your team');
